#ifndef  __LN_KV_KEY_DEF_H__
#define  __LN_KV_KEY_DEF_H__

#define KV_SYSTEM_PARAMETER_MODE			    ("kv_system_parameter_mode")
#define KV_SYSTEM_PARAMETER_CCODE			    ("kv_system_parameter_ccode")
#define KV_SYSTEM_PARAMETER_ADDR			    ("kv_system_parameter_addr")
#define KV_SYSTEM_PARAMETER_AP_ADDR			    ("kv_system_parameter_ap_addr")
#define KV_SYSTEM_PARAMETER_HNAME			    ("kv_system_parameter_hostname")
#define KV_SYSTEM_PARAMETER_AP_HNAME			("kv_system_parameter_ap_hname")
#define KV_SYSTEM_PARAMETER_CONFIG			    ("kv_system_parameter_config")
#define KV_SYSTEM_PARAMETER_AP_CONFIG			("kv_system_parameter_ap_config")
#define KV_SYSTEM_PARAMETER_IP_INFO			    ("kv_system_parameter_ip_info")
#define KV_SYSTEM_PARAMETER_AP_IP_INFO			("kv_system_parameter_ap_ip_info")
#define KV_SYSTEM_PARAMETER_SERVER_CONFIG		("kv_system_parameter_server_config")
#define KV_PSK_INFO                             ("kv_psk_info")
#define KV_OTA_UPG_STATE                        ("kv_ota_upg_state")
#define KV_XTAL_COMP_VAL                        ("kv_xtal_comp_val")
#define KV_TX_POWER_COMP_VAL                    ("kv_tx_power_comp_val")

#endif /* __LN_KV_KEY_DEF_H__ */
